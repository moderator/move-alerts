from disnake.ext import commands
import asyncio
import disnake

class MoveAlertBot(commands.Bot): # Our bot class, this is the bot itself
    def __init__(self):
        # Call parent init method without prefix, Anna's server ID as test guild, no help command, a message cache just to be sure, no intents, and debugging enabled
        super().__init__(command_prefix=disnake.ext.commands.when_mentioned, test_guilds=[683093633934688266], help_command=None, max_messages=10000, 
        intents=disnake.Intents.none(), sync_commands_debug=True)
        # Loads the "Cogs", we only have one though
        self.load_cogs()

    def load_cogs(self):
        for cog in ["cogs.base_cog"]:
            self.load_extension(cog)

if __name__ == "__main__":
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy()) # Sometimes setting this policy, whatever it does, is required on Windows (error otherwise)
    bot = MoveAlertBot() # Create bot
    bot.run("hidden_for_privacy") # Run bot, the string argument is supposed to be your private bot token found in Discord Developer Portal