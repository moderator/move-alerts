import datetime
import json
import ssl
from disnake.ext import commands
from main import MoveAlertBot
import disnake
from disnake.interactions.application_command import ApplicationCommandInteraction
import websockets
import urllib.parse

class BaseCog(commands.Cog, name="Base"):
    def __init__(self, bot: MoveAlertBot):
        self.bot = bot
        self.game_running = False
        self.game_id = None
         # Link to image of starting position chesscom board
        self.starting_fen = "https://www.chess.com/dynboard?fen=rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR%20w%20KQkq%20-%200%201&board=purple&piece=neo&size=3"
        self.socket_url = "wss://www.kingwatcher.com/socket/websocket?vsn=2.0.0" # The websocket URL that will send us moves (if we ask nicely)
        self.flip = False
        self.embed = None
        self.message_to_edit = None

        self.cert = ssl.create_default_context() # The KingWatcher website seems to have some issue with their SSL cert, we make a context to ignore it
        self.cert.check_hostname = False
        self.cert.verify_mode = ssl.CERT_NONE

    @commands.Cog.listener()
    async def on_ready(self):
        await self.bot.change_presence(status=disnake.Status.online, activity="Waiting to follow a game!") # Update Discord status of the bot

    @commands.default_member_permissions(administrator=True) # By default this command is admin-only, can be configured on a per-server basis in the server itself
    @commands.slash_command(description="Starts following a game!") # Indicate this is a Slash Command
    async def startgame(self, inter: ApplicationCommandInteraction, game_id: int, flip: bool): # The function that will make our command do what it does
        if not self.game_running: # No game going on
            self.game_id = game_id # Set a game id (if this changes, the bot will know to stop following the old id)
            self.game_running = True # Make sure we remember we're currently following a game
            self.flip = flip # Remember whether board should be flipped or not
            self.embed = disnake.Embed(title="New Game", description="New moves will be added to this message!") # Create a Discord embed that we'll attach to a message (fancy!)
            self.embed.timestamp = datetime.datetime.now() # Add initial timestamp to the embed, it just makes things look updated
            self.embed.set_image(self.starting_fen + "&flip=true" if flip else "") # Add image of the starting position (potentially flipped)
            self.message_to_edit = await inter.response.send_message(embed=self.embed) # Send and store the message, this way we can edit it later when new moves come in
            await self.bot.change_presence(status=disnake.Status.online, activity=f"Following Game ID {game_id}") # Change Discord status to indicate we are following a game
            await self.run_websocket(game_id) # Call the function that does all the websocket stuff keeping track of the game
        else:
            # Send quick personal message letting user know following a game failed because another one is already being followed.
            await inter.response.send_message("Another game is already going on. Use **/endgame** to end it first.", ephemeral=True)

    @commands.default_member_permissions(administrator=True)
    @commands.slash_command(description="Stops following the game it is currently following.")
    async def stopgame(self, inter: ApplicationCommandInteraction): # Another command, this time to stop following a game
        if self.game_running: # Reset all stuff, half of it probably isn't even really necessary but who carees
            self.game_running = False
            self.game_id = None
            self.embed = None
            self.message_to_edit = None
            self.flip = False
            await self.bot.change_presence(status=disnake.Status.online, activity="Waiting to follow a game!") # Change Discord status once again
            # Send quick personal message letting user know we stopped following, and how they can follow another game now
            await inter.response.send_message("No longer following a game! Please wait a few seconds before using **/startgame** *[game_id]* *[flip]* just to be sure.", 
            ephemeral=True)

        else:
            # Send quick personal message letting user know we already weren't following anything
            await inter.response.send_message("No games are currently being followed, you're free to follow one using **/startgame** *[game_id]* *[flip]*")

    
    async def run_websocket(self, game_id: int): # Nice name for a function
        # Connect to the KingWatcher websocket. The game ID is hardcoded right now because I was still experimenting lol
        # Note: All the phoenix/phx stuff is alien to me, I just looked at what their website sends to the socket and blindly copied it with some improvisation. It works.
        async with websockets.connect(self.socket_url, ssl=self.cert) as websocket:
            # Send an initial hello to the websocket, which includes a game ID and an indication we want to join
            init_packet = ["3", "3", f"game:{self.game_id}", "phx_join", {}]
            await websocket.send(json.dumps(init_packet)) # Send the packet/message we made
            recv_count = 0 # The website sends a heartbeat packet every 13 messages, so we will keep count and do the same after every 14
            hb_count = 4 # For some reason heartbeats start at number 4.. They increase by 1 each time, here we keep track
            while game_id == self.game_id:
                recv_count += 1
                recv: str = await websocket.recv()
                decoded = json.loads(recv) # They use a lame-ish JSON format so we turn the received data into a JSON object we can mess with
                if 'movetext' in decoded[4]: # If the 'movetext' key is in the received data, it means we received a new move
                    print(decoded[4]['movetext']) # Print the move for debugging purposes, once testing is done this will be removed
                    fen = decoded[4]['fen'] # The data also sends us the FEN
                    # We use Chess.com's board image tool by inserting the FEN into the link. We'll get a neat picture back!
                    url = f"https://www.chess.com/dynboard?fen={urllib.parse.quote_plus(fen)}&board=purple&piece=neo&size=3" + "&flip=true" if self.flip else ""
                    self.embed.set_image(url) # Add the image to the embed that we will send along with our Discord message (it looks fancier)
                    self.embed.description = f"A new move has been played! The move was {decoded[4]['movetext']}" # Update embed description to include latest move
                    self.embed.title = "New Move Alert" # Change the title because it is potentially still saying something else
                    self.embed.timestamp = datetime.datetime.now() # Update the timestamp on the embed, this makes it easier to see when the last move was played
                    await self.message_to_edit.edit(embed=self.embed) # Needs testing

                if recv_count % 13 == 0: # Do this every 13th receive
                    # Again, it's just what I saw the website send to the websocket. I haven't even tried to see if the heartbeat is required for the connection to stay alive..
                    # TODO: Check if None actually JSON serializes to null, but it should
                    heartbeat_packet = [None, str(hb_count), "phoenix", "heartbeat", {}]
                    await websocket.send(json.dumps(heartbeat_packet)) # Send the heartbeat packet (supposedly, this lets the server know we're still interested)
                    hb_count += 1 # Increase the number of heartbeat packets we've sent



def setup(bot: MoveAlertBot):
    bot.add_cog(BaseCog(bot)) # Add this "Cog" to the bot
    print("Loaded: BaseCog")